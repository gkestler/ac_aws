#include <stdio.h>
#include <stdbool.h>

int main()
{
	int int_value = 55;
	float float_value = 12.32232;
	bool bool_value = true;
	char str_value[] = "Hello";

	printf("%d %f %d %s\n", int_value, float_value, bool_value, str_value);
}
