#!/bin/bash
filename="./users_passwds.txt"

while IFS='' read -r line;
do
	line=($line)
	user=${line[0]}
	passwd=${line[1]}

	sudo deluser $user
	sudo rm -r "/home/$user/"

done < "$filename"
