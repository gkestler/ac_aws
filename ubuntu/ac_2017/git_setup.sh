#! /bin/bash

LABS_PATH="/home/gkestler/assignments"

cd "$LABS_PATH"
touch tmp
git init
git add tmp
git commit -m "Initial commit"
git remote add origin git@gitlab.com:gkestler/ac_labs.git
git remote -v

#Only do this on first init
if [ ! -f "$HOME/.ssh/id_rsa" ]; then
	ssh-keygen -t rsa -b 4096 -C "gradykestler@gmail.com"
else
	echo "keygen already run: $HOME/.ssh/id_rsa"
fi

if [ ! -f "/etc/init.d/update_git" ]; then
	#Setup update_git service
	sudo ln -s "/home/ubuntu/ac_2017/update_git_serv" "/etc/init.d/update_git"
	sudo update-rc.d update_git defaults
else
	echo "Service update-git already created at /etc/init.d/update_git"
fi

echo ""
echo "Now run eval $ (ssh-agent -s); ssh-add -K ~/.ssh/id_rsa"
