#! /bin/bash


#Update the package manager

sudo usermod -aG sudo ubuntu

echo "***************************Updating Package Manger***************************"
sleep 1
sudo apt-get update

#Install git
echo "***************************Install Git***************************"
sleep 1
sudo apt-get install git

#Make the server directory
echo "***************************Creating Server Directory***************************"
sleep 1
sudo mkdir /srv/jupyterhub; sudo chown -R ubuntu:ubuntu /srv/jupyterhub

#Install Docker
echo "***************************Installing Docker***************************"
sleep 1
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
sudo apt-get update; sudo apt-get upgrade docker-engine
sudo service docker restart

echo "*********************Testing Docker Installation*******************"
sleep 1
sudo docker run hello-world

#Install Jupyterhub, Python3 and other dependencies
echo "*********************Install Jupyterhub and Other Dependencies*******************"
sleep 1
sudo apt-get install python3-pip npm nodejs-legacy
sudo npm install -g configurable-http-proxy
sudo pip3 install jupyterhub
sudo pip3 install --upgrade notebook

#Setup python2.7
echo "*********************Python2.7 Kernel Setup*******************"
sleep 1
sudo apt-get install python-pip python-dev build-essential
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv
sleep 1
sudo python2 -m pip install update
sudo python2 -m pip install --upgrade ipykernel
sudo python2 -m ipykernel install

