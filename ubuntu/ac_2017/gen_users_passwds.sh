#!/bin/bash
filename="./users_passwds.txt"

while IFS='' read -r line;
do
	line=($line)
	user=${line[0]}
	passwd=${line[1]}

	sudo adduser --disabled-password --quiet --gecos "" $user 
	sudo mkdir -m=777 "/home/$user/assignments"
	echo "$user:$passwd" | sudo chpasswd

done < "$filename"
