INSTR1=gkestler
INSTR2=kadriany

declare -a INSTRUCTORS=("gkestler" "kadriany")

##for INSTR in ${INSTRUCTORS[@]}; do
#	if [ -d /home/$INSTR ]; then
#		sudo rm -r /home/$INSTR
#		sudo deluser $INSTR
#	fi
#done

sudo mkdir ~/logs

sudo adduser --disabled-password --quiet --gecos "" gkestler
sudo mkdir -m=777 "/home/gkestler/assignments"
echo "gkestler:kyle" | sudo chpasswd

sudo adduser --disabled-password --quiet --gecos "" kadriany
sudo ln -s "/home/gkestler/assignments" "/home/kadriany/assignments"
echo "kadriany:grady" | sudo chpasswd

LABS_PATH="/home/gkestler/assignments"
AC_PATH="$HOME/ac_2017"

sudo ln -s "$AC_PATH/start_server_script" "$HOME/start_server_script"
sudo ln -s "$AC_PATH/kill_server_script" "$HOME/kill_server_script"
sudo ln -s "$AC_PATH/jupyterhub_config.py" "$HOME/jupyterhub_config.py"

sudo ln -s "$AC_PATH/update_git_script" "$LABS_PATH/.update_git_script"

sudo ln -s "$AC_PATH/dist_assignments.sh" "$LABS_PATH/.dist_assignments.sh"
sudo ln -s "$AC_PATH/del_assignments.sh" "$LABS_PATH/.del_assignments.sh"

if [ ! -f "/etc/init.d/jup_server" ]; then
	#Setup update_git service
	sudo ln -s "/home/ubuntu/ac_2017/server_serv" "/etc/init.d/jup_server"
	sudo update-rc.d jup_server defaults
else
	echo "Service jup_server already created at /etc/init.d/jup_server"
fi
