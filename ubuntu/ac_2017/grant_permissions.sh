#! /bin/bash

filename="./users_passwds.txt"

while IFS='' read -r line;
do
	line=($line)
	user=${line[0]}
	passwd=${line[1]}

	echo "Setting permissions for $user"

	cd /home
	sudo setfacl -b $user
	sudo chmod -R 755 $user
	sudo setfacl -R -m u:$user:rwx $user
	sudo setfacl -d -m u:$user:rwx $user
done < $filename

cd /home

sudo setfacl -b gkestler
sudo chmod -R 755 gkestler
sudo setfacl -R -m u:gkestler:rwx gkestler
sudo setfacl -d -m u:gkestler:rwx gkestler
sudo setfacl -R -m u:kadriany:rwx gkestler
sudo setfacl -d -m u:kadriany:rwx gkestler

sudo setfacl -b kadriany
sudo chmod -R 755 kadriany
sudo setfacl -R -m u:gkestler:rwx kadriany
sudo setfacl -d -m u:gkestler:rwx kadriany
sudo setfacl -R -m u:kadriany:rwx kadriany
sudo setfacl -d -m u:kadriany:rwx kadriany

cd /home
getfacl *
