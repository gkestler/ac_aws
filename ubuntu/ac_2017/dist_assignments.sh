#!/bin/bash
if [ "$1" = "-f" ]; then
	dist_file="$2"
else
	dist_file="$1"
fi

user_list="/home/ubuntu/ac_2017/users_passwds.txt"

while IFS='' read -r line;
do
	line=($line)
	user=${line[0]}
		
	user_assignments="/home/$user/assignments/"

	if [ "$1" = "-f" ]; then
		cp -r "$dist_file" "$user_assignments"
	else
		cp "$dist_file" "$user_assignments"
	fi
	
done < "$user_list"
