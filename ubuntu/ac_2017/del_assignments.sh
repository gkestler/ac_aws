#!/bin/bash
if [ "$1" = "-f" ]; then
	dist_file="$2"
else
	dist_file="$1"
fi

user_list="/home/ubuntu/ac_2017/users_passwds.txt"

while IFS='' read -r line;
do
	line=($line)
	user=${line[0]}
		
	user_assignment="/home/$user/assignments/$dist_file"

	if [ "$1" = "-f" ]; then
		rm -r "$user_assignment"
	else
		rm "$user_assignment"
	fi
	
done < "$user_list"
